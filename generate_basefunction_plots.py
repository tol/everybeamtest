import numpy as np
from matplotlib import pyplot as plt
from generate_oskar_csv import generate_oskar_csv
import run_oskar
from read_oskar_beams import read_oskar_beams
import subprocess
import shutil
from astropy.io import fits


l_max = 7

plt.figure(figsize=(10,6))

for em_idx in range(2):
#for em_idx in [0]:

    for basefunction_idx in range(l_max * (l_max+2)):
    #for basefunction_idx in [0]:
        generate_oskar_csv(basefunction_idx,em_idx)

        # run oskar

        run_oskar.main()

        #d1 = fits.getdata('basefunctions_050_MHz_S0000_TIME_SEP_CHAN_SEP_PHASE_XX.fits')[0,0]
        #d2 = fits.getdata('basefunctions_050_MHz_S0000_TIME_SEP_CHAN_SEP_PHASE_XY.fits')[0,0]

        plt.clf()

        #plt.subplot(1,2,1)
        #plt.imshow(d1/np.pi*180)
        #plt.colorbar()

        #plt.subplot(1,2,2)
        #plt.imshow(d2/np.pi*180)
        #plt.colorbar()

        #plt.savefig('phases{}'.format(basefunction_idx))

        A = read_oskar_beams()
        N = A.shape[0]

        print(N)

        v = np.zeros((N,N,4))

        for i in range(N):
            x = 2.0*i/(N-1) - 1.0
            for j in range(N):
                y = 2.0*j/(N-1) - 1.0
                theta = np.arcsin(np.sqrt(x*x + y*y));
                phi = np.arctan2(y,x);

                e_theta = np.array([np.cos(phi), np.sin(phi)])
                e_phi  = np.array([-np.sin(phi), np.cos(phi)])

                T = np.stack((e_theta, e_phi))
                v[i,j,0] = np.abs(np.dot(A[i,j,:,:], T))[0,0]
                v[i,j,1] = np.abs(np.dot(A[i,j,:,:], T))[0,1]
                v[i,j,2] = np.angle(np.dot(A[i,j,:,:],T))[0,0]
                v[i,j,3] = np.angle(np.dot(A[i,j,:,:],T))[0,1]

        plt.subplot(2,4,1)
        plt.imshow(v[:,:,0].T, clim=(0,.25), origin='lower')
        #plt.imshow(v[:,:,0].T, origin='lower')
        plt.colorbar()
        plt.title('abs(Etheta)')
        plt.ylabel('OSKAR')

        plt.subplot(2,4,2)
        plt.imshow(v[:,:,1].T, clim=(0,.25), origin='lower')
        plt.colorbar()
        plt.title('abs(Ephi)')

        plt.subplot(2,4,3)
        plt.imshow(v[:,:,2].T, clim=(-np.pi, np.pi), cmap='twilight', origin='lower')
        plt.colorbar()
        plt.title('angle(Etheta)')

        plt.subplot(2,4,4)
        plt.imshow(v[:,:,3].T, clim=(-np.pi, np.pi), cmap='twilight', origin='lower')
        plt.colorbar()
        plt.title('angle(Ephi)')

        # run EveryBeam

        #generate_oskar_csv(basefunction_idx, em_idx)

        l = int(np.sqrt(basefunction_idx+1))
        m = basefunction_idx-l*l+1-l
        s = em_idx

        generate_oskar_csv(l*l-1+l-m, em_idx)

        subprocess.call(["/home/vdtol/src/EveryBeam/oskar/oskar_csv_to_hdf5.py", "telescope.tm", "oskar.h5"])

        shutil.copy("oskar.h5", "/home/vdtol/share/everybeam")

        subprocess.call(['./everybeamtest'])

        A = np.load('response.npy')

        A *= (-1)**(m+1)

        N = A.shape[0]

        print(N)

        v = np.zeros((N,N,4))

        for i in range(N):
            x = 2.0*i/(N-1) - 1.0
            for j in range(N):
                y = 2.0*j/(N-1) - 1.0
                theta = np.arcsin(np.sqrt(x*x + y*y));
                phi = np.arctan2(y,x);

                e_theta = np.array([np.cos(phi), np.sin(phi)])
                e_phi  = np.array([-np.sin(phi), np.cos(phi)])

                T = np.stack((e_theta, e_phi))
                T = np.eye(2)
                v[i,j,0] = np.abs(np.dot(A[i,j,:,:], T))[0,0]
                v[i,j,1] = np.abs(np.dot(A[i,j,:,:], T))[0,1]
                v[i,j,2] = np.angle(np.dot(A[i,j,:,:],T))[0,0]
                v[i,j,3] = np.angle(np.dot(A[i,j,:,:],T))[0,1]

        plt.subplot(2,4,5)
        plt.imshow(v[:,:,0].T, clim=(0,.25), origin='lower')
        plt.colorbar()
        plt.title('abs(Etheta)')
        plt.ylabel('EveryBeam')

        plt.subplot(2,4,6)
        plt.imshow(v[:,:,1].T, clim=(0,.25), origin='lower')
        plt.colorbar()
        plt.title('abs(Ephi)')

        plt.subplot(2,4,7)
        plt.imshow(v[:,:,2].T, clim=(-np.pi, np.pi), cmap='twilight', origin='lower')
        plt.colorbar()
        plt.title('angle(Ex)')

        plt.subplot(2,4,8)
        plt.imshow(v[:,:,3].T, clim=(-np.pi, np.pi), cmap='twilight', origin='lower')
        plt.colorbar()
        plt.title('angle(Ey)')

        #with open('telescope.tm/element_pattern_spherical_wave_x_te_re_0_50.txt') as f:
            #coeff_line = f.readline()
            #plt.gcf().suptitle('telescope.tm/element_pattern_spherical_wave_x_te_re_0_50.txt\n{}'.format(coeff_line))

        plt.gcf().suptitle('l = {}, m = {}, s = {}'.format(l,m,s))

        plt.savefig('basefunction{}-{}'.format(basefunction_idx,em_idx))
