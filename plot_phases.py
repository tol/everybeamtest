from astropy.io import fits
from matplotlib import pyplot as plt

d1 = fits.getdata('basefunctions_050_MHz_S0000_TIME_SEP_CHAN_SEP_PHASE_XX.fits')[0,0]
d2 = fits.getdata('basefunctions_050_MHz_S0000_TIME_SEP_CHAN_SEP_PHASE_XY.fits')[0,0]

plt.subplot(1,2,1)
plt.imshow(d1/np.pi*180)
colorbar()

plt.subplot(1,2,2)
plt.imshow(d2/np.pi*180)
colorbar()

plt.show()
