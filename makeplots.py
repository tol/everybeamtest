#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt


A = np.load('response.npy')

plt.subplot(2,2,1)
plt.imshow(abs(A[:,:,0,0])**2, clim=(0.0,np.nanmax(abs(A[:,:,0,0])**2)))
print (0.0,np.amax(abs(A[:,:,0,0])**2))
plt.colorbar()
plt.subplot(2,2,2)
plt.imshow(abs(A[:,:,0,1])**2, clim=(0.0,np.nanmax(abs(A[:,:,0,1])**2)))
plt.colorbar()
plt.subplot(2,2,3)
plt.imshow(abs(A[:,:,1,0])**2, clim=(0.0,np.nanmax(abs(A[:,:,1,0])**2)))
plt.colorbar()
plt.subplot(2,2,4)
plt.imshow(abs(A[:,:,1,1])**2, clim=(0.0,np.nanmax(abs(A[:,:,1,1])**2)))
plt.colorbar()

plt.savefig('response_amp.png')

plt.figure()

plt.subplot(2,2,1)
plt.imshow(np.angle(A[:,:,0,0])**2)
plt.subplot(2,2,2)
plt.imshow(np.angle(A[:,:,0,1])**2)
plt.subplot(2,2,3)
plt.imshow(np.angle(A[:,:,1,0])**2)
plt.subplot(2,2,4)
plt.imshow(np.angle(A[:,:,1,1])**2)

plt.savefig('response_phase.png')
