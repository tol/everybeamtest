from matplotlib import pyplot as plt

from astropy.io import fits

import numpy as np

def read_oskar_beams():
    A = None
    for i, pol1 in enumerate(['X', 'Y']):
        for j, pol2 in enumerate(['X', 'Y']):
            filename_amp = 'basefunctions_050_MHz_S0000_TIME_SEP_CHAN_SEP_AMP_{}{}.fits'.format(pol1, pol2)
            filename_phase = 'basefunctions_050_MHz_S0000_TIME_SEP_CHAN_SEP_PHASE_{}{}.fits'.format(pol1, pol2)
            d = (fits.getdata(filename_amp) * np.exp(1j*fits.getdata(filename_phase)))[0,0,:,:].T
            if A is None:
                N = d.shape[-1]
                A = np.zeros((N,N,2,2), dtype=np.complex128)
            A[:,:,i,j] = d

    return A



if __name__ == "__main__":


    A = read_oskar_beams()
    N = A.shape[0]

    print(N)

    v = np.zeros((N,N,4))

    for i in range(N):
        x = 2.0*i/(N-1) - 1.0
        for j in range(N):
            y = 2.0*j/(N-1) - 1.0
            theta = np.arcsin(np.sqrt(x*x + y*y));
            phi = np.arctan2(y,x);

            e_theta = np.array([np.cos(phi), np.sin(phi)])
            e_phi  = np.array([-np.sin(phi), np.cos(phi)])

            T = np.stack((e_theta, e_phi)).T
            v[i,j,0] = np.abs(np.dot(A[i,j,:,:], T))[0,0]
            v[i,j,1] = np.abs(np.dot(A[i,j,:,:], T))[0,1]
            v[i,j,2] = np.mod(np.angle(A[i,j,0,0]),2*np.pi)
            v[i,j,3] = np.angle(A[i,j,0,1])

    plt.subplot(2,2,1)
    plt.imshow(v[:,:,0], clim=(0,.25))
    plt.colorbar()
    plt.title('abs(Etheta)')

    plt.subplot(2,2,2)
    plt.imshow(v[:,:,1], clim=(0,.25))
    plt.colorbar()
    plt.title('abs(Ephi)')

    plt.subplot(2,2,3)
    plt.imshow(v[:,:,2])
    plt.colorbar()
    plt.title('angle(Ex)')

    plt.subplot(2,2,4)
    plt.imshow(v[:,:,3])
    plt.colorbar()
    plt.title('angle(Ey)')

    plt.show()







